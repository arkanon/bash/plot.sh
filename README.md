# plot.sh ([ishortn.ink/plot.sh](http://ishortn.ink/plot.sh))

Script bash com suporte a mouse para desenho em ANSI.

![plot.sh](plotsh.png "plot.sh")

Testado com
- Linux (dentro e fora de uma sessão `screen`):
  - xfce4-terminal
  - gnome-terminal
  - xterm
  - terminator
- FreeBSD:
  - via ssh



## Uso

Um FreeBSD recém instalado pode exigir algumas [configurações específicas](http://gitlab.com/arkanon/misc/doc/-/blob/master/freebsd/README.md).

- Clone o repositório git
  ```shell-session
  $ mkdir -p ~/git
  $ cd ~/git
  $ git clone https://gitlab.com/arkanon/bash/plot.sh
  ```
- Execute o script
  ```shell-session
  $ ./plot.sh
  ```
- selecione uma cor clicando sobre ela com o botão esquerdo do mouse
- clique na tela de desenho com o botão esquerdo do mouse para pintar um pixel (caractere)
- arraste sem soltar o botão esquerdo para pintar uma área retangular
- clique com o botão direito do mouse para apagar (pintar de preto)
- arraste sem soltar o botão direito para apagar uma área
- saia com `Ctrl-C`

![plot.sh](plotsh.mp4 "plot.sh"){width=100%}



## TODO

- [x] plotar com botão esquerdo
- [x] apagar com botão direito
- [x] tratar ctrl+TECLA em uma função
- [x] mostrar código do mouse
- [x] mostrar coordenadas
- [x] desenhar retângulo arrastando
- [x] desenhar apenas dentro da área limite
- [x] usar cores
- [x] selecionar cor
- [x] compatibilidade com FreeBSD
- [x] reduzir a quantidade de dependências a comandos externos (cat, wc, sed, tput, clear)
- [ ] ao redimensionar a janela, redesenhar o frame



## Dependências

- `bash`
- `stty` Linux: coreutils, FreeBSD: nativo



## Referências

- http://stackoverflow.com/questions/5966903/how-to-get-mousemove-and-mouseclick-in-bash/55437976#55437976
- http://github.com/tinmarino/mouse_xterm/blob/master/README.md
- http://github.com/tinmarino/mouse_xterm/blob/master/mouse.sh
- http://unix.stackexchange.com/questions/88296/get-vertical-cursor-position
- http://invisible-island.net/xterm/ctlseqs/ctlseqs.html#h2-Mouse-Tracking
- http://cfajohnson.com/shell/?2005-07-15_mousetraps
- http://wikipedia.org/wiki/ANSI_escape_code
- http://github.com/telmich/gpm/blob/master/src/prog/display-coords.c

☐
