# plot1.sh

# Arkanon <arkanon@lsd.org.br>
# 2024/05/05 (Sun) 12:10:39 -03
# 2024/05/04 (Sat) 19:08:59 -03
# 2024/05/04 (Sat) 10:20:52 -03
# 2024/05/03 (Fri) 10:26:26 -03

# <http://stackoverflow.com/questions/5966903/how-to-get-mousemove-and-mouseclick-in-bash/55437976#55437976>
# <http://github.com/tinmarino/mouse_xterm/blob/master/mouse.sh>
# <http://unix.stackexchange.com/questions/88296/get-vertical-cursor-position>

  print_lin=$(( LINES-1 ))
  print_col=10 # $(( (COLUMNS-1)/2 ))
       stty=$(stty -g)

      # char2mb 🔴

      # pix=$'\U1f534' # 🔴 \xf0\x9f\x94\xb4
      # pix=$'\U25cf'  # ●  \xe2\x97\x8f
        pix=$'\U2588'  # █  \xe2\x96\x88
      # pix=$'\U2a'    # *  \x2a

  declare -A m_code=(

  ######## \e[ CODE ; COLUMN ; LINE [Mm]

  ######## [vte/xterm+screen/terminator]

     [<0]=l-click # M=press, m=release
     [<1]=m-click
     [<2]=r-click

    [<32]=l-drag
    [<33]=m-drag  # terminator
    [<34]=r-drag

    [<64]=u-scroll
    [<65]=d-scroll

  ######## [xterm]

     [32]=l-click
     [33]=m-click
     [34]=r-click
     [35]=release

     [64]=l-drag
     [65]=m-drag
     [66]=r-drag

     [96]=u-scroll
     [97]=d-scroll

  )



  main()
  (

    init
    trap restore hup int quit tstp

    while true
    do

      unset M cod col lin chr

      while read -rsn1 -t.001; do M+=$REPLY; done

      if [[ $M =~ M ]]
      then

        M=${M%%M*}M
        : "${M//[$'\e'[M]/}"
        : "${_//;/ }"
        read cod col lin <<< $_

        case ${m_code[$cod]} in
          l-* ) chr=$pix ;;
          r-* ) chr=' '  ;;
        esac

        if [[ $chr ]]
        then


          ? ${lin},${col}H
          printf "$chr"
          ? ${print_lin},${print_col}H
          printf "$M" | cat -A
          printf " [$cod] ($lin,$col)"
          ? K

        fi

      fi

    done

  )



  ? ()
  {
    [[ $2 ]] && local var="-v$2"
    printf $var "\e[${1//,/;}"
  }



  char2mb()
  {

    printf "${1?}" |
    od -tx1 -An |
    sed 's/ /\\x/g'

    printf "${1?}" |
    iconv -t UTF-32LE |
    od -tx4 -An |
    sed 's/ 0*/\\U/g'

  }



  init()
  {
    clear
    tput civis
    stty -echo
    ? ?1000,1002,1006,1015h
  }



  restore()
  {
    ? ?1000,1002,1006,1015l
    stty $stty
    tput cnorm
    ? $LINES,1H
    ? K
    exit
  }



  main



# EOF
