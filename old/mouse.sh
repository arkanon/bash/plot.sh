# mouse.sh

# Arkanon <arkanon@lsd.org.br>
# 2024/05/03 (Fri) 10:26:26 -03

# <http://stackoverflow.com/questions/5966903/how-to-get-mousemove-and-mouseclick-in-bash/55437976#55437976>
# <http://github.com/tinmarino/mouse_xterm/blob/master/mouse.sh>
# <http://unix.stackexchange.com/questions/88296/get-vertical-cursor-position>

      m_pos=$'\e[?1000;1002;1006;1015h'
      m_off=$'\e[?1000;1002;1006;1015l'
      c_pos=$'\e[6n'
        esc=$'\e'
      # pix=$'\xf0\x9f\x94\xb4'
        pix=$'\xe2\x97\x8f'
      # pix='*'
  print_lin=$(( LINES-1 ))
  print_col=10 # $(( (COLUMNS-1)/2 ))
       stty=$(stty -g)

  declare -A m_code=(

  ######## \e[ CODE ; COLUMN ; LINE [Mm]

  ######## [vte/xterm+screen/terminator]

     [<0]=l-click # M=press, m=release
     [<1]=m-click
     [<2]=r-click

    [<32]=l-drag
    [<33]=m-drag  # terminator
    [<34]=r-drag

    [<64]=u-scroll
    [<65]=d-scroll

  ######## [xterm]

     [32]=l-click
     [33]=m-click
     [34]=r-click
     [35]=release

     [64]=l-drag
     [65]=m-drag
     [66]=r-drag

     [96]=u-scroll
     [97]=d-scroll

  )



  c_pos()
  {
    local REPLY
  # local c_lin c_col
    echo -n $c_pos
    read -sdR
  # echo -n $REPLY | cat -A
  # echo
    read c_lin c_col <<< ${REPLY//[$esc[;]/ }
  # echo c_lin=$c_lin c_col=$c_col
  }



  m_dbg()
  (
    echo -n $m_pos
    trap 'echo -n $m_off; stty echo; exit' hup int quit tstp
    while true
    do
      while read -d$esc -t0.1
      do
        echo -n "$REPLY" | cat -A
        echo
      done
    done
  )



  m_pos()
  (
    echo -n $m_pos
    tput civis
    stty -echo
    trap 'echo -n $m_off; stty $stty; tput cnorm; exit' hup int quit tstp
    while true
    do
      unset M cod col lin chr
      while read -rsn1 -t.001
      do
        M+=$REPLY
      done
      if [[ $M =~ M ]]
      then
        M=${M%%M*}M
        : "${M//[$esc[M]/}"
        : "${_//;/ }"
        read cod col lin <<< $_
        case ${m_code[$cod]} in
          l-*) chr=$pix ;;
          r-*) chr=' '  ;;
        esac
        if [[ $chr ]]
        then
          echo -n "$esc[${lin};${col}H"
          echo -n "$chr"
          echo -n "$esc[${print_lin};${print_col}H"
          echo -n "$M" | cat -A
          echo -n " ($cod,$lin,$col)"
          echo -n "$esc[K"
        else
          echo -n "$esc[${print_lin};${print_col}H"
          echo -n "$esc[K"
        fi
      fi
    done
  )



  log()
  {
    echo "$@" >> mouse.log
  }



  m_consume_keys()
  {
    local s_consumed
    while read -r -n 1 -t 0 c
    do
      s_consumed+=$c
    done
    log Consumed: $s_consumed
  }



  m_read_cursor_pos()
  {

    # Read cursor_pos <- xterm <- readline
    # Out: gi_cursor_{x,y} 50;1 (x;y) if click on line 1, column 50: starting at 1;1

    local i_row=0 i_col=0  # Cannot be declared as integer. read command would fail
    local row_read

    {
      # clear stdin
      m_consume_keys

      # set echo style (no echo)
      local oldstty=$(stty -g)
      stty raw -echo min 0

      # ask cursor pos
      IFS=';' read -r -dR -p "$c_position" row_read i_col

      # reset echo style (display keystrokes)
      stty $oldstty
    } < /dev/tty

    i_row=${row_read#*[}

    log Pos: x=$i_col $i_row

    if (( $# > 0 ))
    then
      m_bol_x=$i_col
      m_bol_y=$i_row
      log Bol: x=$m_bol_x y=$m_bol_y
    else
      m_cursor_x=$i_col
      m_cursor_y=$i_row
      log Cursor: x=$m_cursor_x y=$m_cursor_y
    fi
  }



  run()
  {

    . mouse.sh
    echo -n $m_pos; while true; do m_read_cursor_pos; sleep .1; done

    stty 4500:5:bf:8a3b:3:1c:7f:15:4:0:1:0:11:13:1a:0:12:f:17:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0
    echo -n $m_off

  }



# EOF
