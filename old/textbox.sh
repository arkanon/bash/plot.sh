# textbox.sh

# Arkanon <arkanon@lsd.org.br>
# 2024/05/08 (Wed) 10:46:47 -03



  . border.sh



  ? ()
  {
    [[ $2 ]] && local var="-v$2"
    printf $var "\e[${1//,/;}"
  }



  textbox() # texto linha coluna
  {
    local theme b_{h,v,lt,rt,lb,rb} MAPFILE
    theme=simple_round_1
  # theme=double_square
    eval  b_h="\${$theme[h]}"
    eval  b_v="\${$theme[v]}"
    eval b_lt="\${$theme[lt]}"
    eval b_rt="\${$theme[rt]}"
    eval b_lb="\${$theme[lb]}"
    eval b_rb="\${$theme[rb]}"
    mapfile <<< $1
    local i bordah altura=${#MAPFILE[*]} largura=$(sed -r $'s/\e[^m]+m//g' <<< $1 | wc -L)
    local linha=${2:-$(((LINES-altura)/2+1))} coluna=${3:-$(((COLUMNS-largura)/2-1))}
    printf -v bordah "%$((largura+2))s" # cria uma sequencia de $largura espaços
    bordah=${bordah// /$b_h} # transforma a sequencia de espaços em uma borda horizontal
    ? s    # salva a posição do cursor
    ? ?25l # torna o cursor invisível
    ? "${linha},${coluna}H$b_lt$bordah$b_rt"
    for ((i=1; i<altura; i++))
    {
      ? "$((linha+i)),${coluna}H$b_v ${MAPFILE[i]}"
      ? "$((linha+i)),$((coluna+largura+3))H$b_v"
    }
    ? "$((linha+i)),${coluna}H$b_lb$bordah$b_rb"
    ? u    # restaura a posição do cursor
    ? ?25h # torna o cursor visível
  }



  ls=$(ll)

  echo
  time sed -r $'s/\x1b[^m]+m//g' <<< $ls
  echo
  time echo "${ls//$'\x1b'+([[0-9;])m/}"
  echo

  textbox "$ls"



# EOF
