# border.sh

# Arkanon <arkanon@lsd.org.br>
# 2024/05/05 (Sun) 23:16:33 -03

# <http://en.wikipedia.org/wiki/Box-drawing_characters>



  declare -A   simple_bold_1=( [h]=━ [lh]=╺ [rh]=╸ [v]=│ [tv]=╷ [bv]=╵ [lt]=┍ [rt]=┑ [lb]=┕ [rb]=┙ [m]=┿ [lm]=┝ [rm]=┥ [mt]=┯ [mb]=┷ )
  declare -A   simple_bold_2=( [h]=╍ [lh]=╺ [rh]=╸ [v]=╎ [tv]=╷ [bv]=╵ [lt]=┍ [rt]=┑ [lb]=┕ [rb]=┙ [m]=┿ [lm]=┝ [rm]=┥ [mt]=┯ [mb]=┷ )
  declare -A   simple_bold_3=( [h]=┅ [lh]=╺ [rh]=╸ [v]=┆ [tv]=╷ [bv]=╵ [lt]=┍ [rt]=┑ [lb]=┕ [rb]=┙ [m]=┿ [lm]=┝ [rm]=┥ [mt]=┯ [mb]=┷ )
  declare -A   simple_bold_4=( [h]=┉ [lh]=╺ [rh]=╸ [v]=┊ [tv]=╷ [bv]=╵ [lt]=┍ [rt]=┑ [lb]=┕ [rb]=┙ [m]=┿ [lm]=┝ [rm]=┥ [mt]=┯ [mb]=┷ )

  declare -A   bold_square_1=( [h]=━ [lh]=╺ [rh]=╸ [v]=┃ [tv]=╻ [bv]=╹ [lt]=┏ [rt]=┓ [lb]=┗ [rb]=┛ [m]=╋ [lm]=┣ [rm]=┫ [mt]=┳ [mb]=┻ )
  declare -A   bold_square_2=( [h]=╍ [lh]=╺ [rh]=╸ [v]=╏ [tv]=╻ [bv]=╹ [lt]=┏ [rt]=┓ [lb]=┗ [rb]=┛ [m]=╋ [lm]=┣ [rm]=┫ [mt]=┳ [mb]=┻ )
  declare -A   bold_square_3=( [h]=┅ [lh]=╺ [rh]=╸ [v]=┇ [tv]=╻ [bv]=╹ [lt]=┏ [rt]=┓ [lb]=┗ [rb]=┛ [m]=╋ [lm]=┣ [rm]=┫ [mt]=┳ [mb]=┻ )
  declare -A   bold_square_4=( [h]=┉ [lh]=╺ [rh]=╸ [v]=┋ [tv]=╻ [bv]=╹ [lt]=┏ [rt]=┓ [lb]=┗ [rb]=┛ [m]=╋ [lm]=┣ [rm]=┫ [mt]=┳ [mb]=┻ )

  declare -A   bold_simple_1=( [h]=─ [lh]=╶ [rh]=╴ [v]=┃ [tv]=╻ [bv]=╹ [lt]=┎ [rt]=┒ [lb]=┖ [rb]=┚ [m]=╂ [lm]=┠ [rm]=┨ [mt]=┰ [mb]=┸ )
  declare -A   bold_simple_2=( [h]=╌ [lh]=╶ [rh]=╴ [v]=╏ [tv]=╻ [bv]=╹ [lt]=┎ [rt]=┒ [lb]=┖ [rb]=┚ [m]=╂ [lm]=┠ [rm]=┨ [mt]=┰ [mb]=┸ )
  declare -A   bold_simple_3=( [h]=┄ [lh]=╶ [rh]=╴ [v]=┇ [tv]=╻ [bv]=╹ [lt]=┎ [rt]=┒ [lb]=┖ [rb]=┚ [m]=╂ [lm]=┠ [rm]=┨ [mt]=┰ [mb]=┸ )
  declare -A   bold_simple_4=( [h]=┈ [lh]=╶ [rh]=╴ [v]=┋ [tv]=╻ [bv]=╹ [lt]=┎ [rt]=┒ [lb]=┖ [rb]=┚ [m]=╂ [lm]=┠ [rm]=┨ [mt]=┰ [mb]=┸ )

  declare -A  simple_round_1=( [h]=─ [lh]=╶ [rh]=╴ [v]=│ [tv]=╷ [bv]=╵ [lt]=╭ [rt]=╮ [lb]=╰ [rb]=╯ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ )
  declare -A  simple_round_2=( [h]=╌ [lh]=╶ [rh]=╴ [v]=╎ [tv]=╷ [bv]=╵ [lt]=╭ [rt]=╮ [lb]=╰ [rb]=╯ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ )
  declare -A  simple_round_3=( [h]=┄ [lh]=╶ [rh]=╴ [v]=┆ [tv]=╷ [bv]=╵ [lt]=╭ [rt]=╮ [lb]=╰ [rb]=╯ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ )
  declare -A  simple_round_4=( [h]=┈ [lh]=╶ [rh]=╴ [v]=┊ [tv]=╷ [bv]=╵ [lt]=╭ [rt]=╮ [lb]=╰ [rb]=╯ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ )

  declare -A simple_square_1=( [h]=─ [lh]=╶ [rh]=╴ [v]=│ [tv]=╷ [bv]=╵ [lt]=┌ [rt]=┐ [lb]=└ [rb]=┘ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ )
  declare -A simple_square_2=( [h]=╌ [lh]=╶ [rh]=╴ [v]=╎ [tv]=╷ [bv]=╵ [lt]=┌ [rt]=┐ [lb]=└ [rb]=┘ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ )
  declare -A simple_square_3=( [h]=┄ [lh]=╶ [rh]=╴ [v]=┆ [tv]=╷ [bv]=╵ [lt]=┌ [rt]=┐ [lb]=└ [rb]=┘ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ )
  declare -A simple_square_4=( [h]=┈ [lh]=╶ [rh]=╴ [v]=┊ [tv]=╷ [bv]=╵ [lt]=┌ [rt]=┐ [lb]=└ [rb]=┘ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ )

  declare -A double_simple_1=( [h]=─ [lh]=╶ [rh]=╴ [v]=║ [tv]=  [bv]=  [lt]=╓ [rt]=╖ [lb]=╙ [rb]=╜ [m]=╫ [lm]=╟ [rm]=╢ [mt]=╥ [mb]=╨ )
  declare -A double_simple_2=( [h]=╌ [lh]=╶ [rh]=╴ [v]=║ [tv]=  [bv]=  [lt]=╓ [rt]=╖ [lb]=╙ [rb]=╜ [m]=╫ [lm]=╟ [rm]=╢ [mt]=╥ [mb]=╨ )
  declare -A double_simple_3=( [h]=┄ [lh]=╶ [rh]=╴ [v]=║ [tv]=  [bv]=  [lt]=╓ [rt]=╖ [lb]=╙ [rb]=╜ [m]=╫ [lm]=╟ [rm]=╢ [mt]=╥ [mb]=╨ )
  declare -A double_simple_4=( [h]=┈ [lh]=╶ [rh]=╴ [v]=║ [tv]=  [bv]=  [lt]=╓ [rt]=╖ [lb]=╙ [rb]=╜ [m]=╫ [lm]=╟ [rm]=╢ [mt]=╥ [mb]=╨ )

  declare -A simple_double_1=( [h]=═ [lh]=  [rh]=  [v]=│ [tv]=╷ [bv]=╵ [lt]=╒ [rt]=╕ [lb]=╘ [rb]=╛ [m]=╪ [lm]=╞ [rm]=╡ [mt]=╤ [mb]=╧ )
  declare -A simple_double_2=( [h]=═ [lh]=  [rh]=  [v]=╎ [tv]=╷ [bv]=╵ [lt]=╒ [rt]=╕ [lb]=╘ [rb]=╛ [m]=╪ [lm]=╞ [rm]=╡ [mt]=╤ [mb]=╧ )
  declare -A simple_double_3=( [h]=═ [lh]=  [rh]=  [v]=┆ [tv]=╷ [bv]=╵ [lt]=╒ [rt]=╕ [lb]=╘ [rb]=╛ [m]=╪ [lm]=╞ [rm]=╡ [mt]=╤ [mb]=╧ )
  declare -A simple_double_4=( [h]=═ [lh]=  [rh]=  [v]=┊ [tv]=╷ [bv]=╵ [lt]=╒ [rt]=╕ [lb]=╘ [rb]=╛ [m]=╪ [lm]=╞ [rm]=╡ [mt]=╤ [mb]=╧ )

  declare -A   double_square=( [h]=═ [lh]=  [rh]=  [v]=║ [tv]=  [bv]=  [lt]=╔ [rt]=╗ [lb]=╚ [rb]=╝ [m]=╬ [lm]=╠ [rm]=╣ [mt]=╦ [mb]=╩ )



  #  🬀   🬁   🬂   🬃   🬄   🬅   🬆   🬇   🬈   🬉   🬊   🬋   🬌   🬍   🬎   🬏
  #
  #  🬐   🬑   🬒   🬓   🬔   🬕   🬖   🬗   🬘   🬙   🬚   🬛   🬜   🬝   🬞   🬟
  #
  #  🬠   🬡   🬢   🬣   🬤   🬥   🬦   🬧   🬨   🬩   🬪   🬫   🬬   🬭   🬮   🬯
  #
  #  🬰   🬱   🬲   🬳   🬴   🬵   🬶   🬷   🬸   🬹   🬺   🬻   🬼   🬽   🬾   🬿
  #
  #  🭀   🭁   🭂   🭃   🭄   🭅   🭆   🭇   🭈   🭉   🭊   🭋   🭌   🭍   🭎   🭏
  #
  #  🭐   🭑   🭒   🭓   🭔   🭕   🭖   🭗   🭘   🭙   🭚   🭛   🭜   🭝   🭞   🭟
  #
  #  🭠   🭡   🭢   🭣   🭤   🭥   🭦   🭧   🭨   🭩   🭪   🭫   🭬   🭭   🭮   🭯
  #
  #  🭰   🭱   🭲   🭳   🭴   🭵   🭶   🭷   🭸   🭹   🭺   🭻   🭼   🭽   🭾   🭿
  #
  #  🮀   🮁   🮂   🮃   🮄   🮅   🮆   🮇   🮈   🮉   🮊   🮋   🮌   🮍   🮎   🮏
  #
  #  🮐   🮑   🮒     🮔   🮕   🮖   🮗   🮘   🮙   🮚   🮛   🮜   🮝   🮞   🮟
  #
  #  🮠   🮡   🮢   🮣   🮤   🮥   🮦   🮧   🮨   🮩   🮪   🮫   🮬   🮭   🮮   🮯
  #
  #  🮰   🮱   🮲   🮳   🮴   🮵   🮶   🮷   🮸   🮹   🮺   🮻   🮼   🮽   🮾   🮿
  #
  #  🯀   🯁   🯂   🯃   🯄   🯅   🯆   🯇   🯈   🯉   🯊
  #
  #  🯰   🯱   🯲   🯳   🯴   🯵   🯶   🯷   🯸   🯹



  #  ▀  ▁  ▂  ▃  ▄  ▅  ▆  ▇  █  ▉  ▊  ▋  ▌  ▍  ▎  ▏
  #
  #  ▐  ░  ▒  ▓  ▔  ▕  ▖  ▗  ▘  ▙  ▚  ▛  ▜  ▝  ▞  ▟



  #  ┟  ┧  ┞  ┦  ┵  ┶  ┭  ┮  ╁  ╀  ┽  ┾
  #
  #  ┡  ┩  ┢  ┪  ┺  ┹  ┲  ┱  ╇  ╈  ╊  ╉
  #
  #  ╄  ╃  ╆  ╅  ╾  ╼  ╽  ╿
  #
  #  ╱  ╲  ╳



# EOF
