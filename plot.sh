#!/usr/bin/env bash

# plot.sh

# Arkanon <arkanon@lsd.org.br>
# 2024/05/25 (Sat) 02:14:59 -03
# 2024/05/11 (Sat) 08:16:32 -03
# 2024/05/10 (Fri) 20:32:29 -03
# 2024/05/08 (Wed) 13:26:47 -03
# 2024/05/07 (Tue) 16:17:35 -03
# 2024/05/07 (Tue) 04:01:09 -03
# 2024/05/06 (Mon) 21:34:16 -03
# 2024/05/06 (Mon) 00:10:43 -03
# 2024/05/05 (Sun) 12:15:36 -03



  # se este script for executado, a função main será automaticamente chamada.
  # se for lido com source, as funções serão apenas importadas para execução manual e eventual debug
  [[ $0 == $BASH_SOURCE ]] && autoexec=true || autoexec=false



  main()
  (

    unset color

    conf
    init
    frame
    palette

    trap restore hup int quit tstp

    while true
    do

      unset M cod col lin chr

      while read -rsn1 -t.001; do M+=$REPLY; done

      if [[ $M =~ $'\e'.+M ]]
      then

        M=${M%%M*}M
        : "${M//[$'\x1b\x5b'M]/}"
        : "${_//;/ }"
        read cod col lin <<< $_

        if [[ ${mcode[$cod]} == l-click ]] &&
           (( lin==print_lin && col>=COLUMNS-17+2*1 && col<=COLUMNS-17+2*7+1 ))
        then
          color=$(( (2*7-(COLUMNS-col-1))/2+1 ))
          palette
          ? ${print_lin},$((COLUMNS-17+2*color))H
          ? 30,4${color}m
          printf "$selshape"
          ? 0m
        fi

        case ${mcode[$cod]} in
          l-* ) cor=4${color:-$defcolor} ;;
          r-* ) cor=40 ;;
        esac

        case ${mcode[$cod]} in
          *-click ) li=$lin ci=$col lf=$lin cf=$col ;;
          *-drag  ) lf=$lin cf=$col ;;
        esac

        l0=$li l1=$lf && ((li>lf)) && l0=$lf l1=$li
        c0=$ci c1=$cf && ((ci>cf)) && c0=$cf c1=$ci

        for (( l=l0 ; l<=l1 ; l++ ))
        {
          for (( c=c0 ; c<=c1 ; c++ ))
          {
            if ((
                  l>=t_edge && l<=LINES  -b_edge+1 &&
                  c>=l_edge && c<=COLUMNS-r_edge+1
               ))
            then
              ? ${l},${c}H
              ? ${cor}m
              printf ' '
              ? 0m
            fi
          }
        }

        ? ${print_lin},3H
        printf "${M/$'\x1b'/^[}"
        printf " [$cod] ($li,$ci) $arrow ($lf,$cf) $cor     "

      fi

    done

  )



  conf()
  {

        title=plotsh

         stty=$(stty -g)

    read LINES COLUMNS < <(stty size)

    print_lin=$(( LINES-1 ))

       l_edge=3
       r_edge=3

       b_edge=4
       t_edge=4

     defcolor=7

     # 🮘 # terminal unicode
     # ■ # console freebsd
     [[ $DISPLAY ]] && selshape='🮘🮘' || selshape='■■'
   # selshape='▲▼' # console freebsd
   # selshape='⦿ '
   # selshape='◀▶'
   # selshape='●●'

        arrow='→'

    declare -Ag theme=( [h]=─ [lh]=╶ [rh]=╴ [v]=│ [tv]=╷ [bv]=╵ [lt]=┌ [rt]=┐ [lb]=└ [rb]=┘ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ ) # simple_square_1
  # declare -Ag theme=( [h]=─ [lh]=╶ [rh]=╴ [v]=│ [tv]=╷ [bv]=╵ [lt]=╭ [rt]=╮ [lb]=╰ [rb]=╯ [m]=┼ [lm]=├ [rm]=┤ [mt]=┬ [mb]=┴ ) # simple_round_1

    declare -Ag mcode=(

    ######## \x1b[CODE;COLUMN;LINE[Mm] [xterm compatible terminal]
    ######## [ !"]CiLi#CfLf            [freebsd moused]

    ######## [vte/xterm+screen/terminator]

       [<0]=l-click # M=press, m=release
       [<1]=m-click
       [<2]=r-click

      [<32]=l-drag
      [<33]=m-drag  # terminator
      [<34]=r-drag

      [<64]=u-scroll
      [<65]=d-scroll

    ######## [xterm]

       [32]=l-click
       [33]=m-click
       [34]=r-click
       [35]=release

       [64]=l-drag
       [65]=m-drag
       [66]=r-drag

       [96]=u-scroll
       [97]=d-scroll

    )

  # if [[ $OSTYPE =~ bsd ]]
  # then
  #   which gcat >& /dev/null || { echo "Por favor, instale o pacote coreutils" >&2; exit 1; }
  #   gnu=g
  # fi

  }



  init()
  {
    ? 2J   # clear
    ? H    # home
    ? ?25l # torna o cursor invisível
    stty -echo
    ? ?1000,1002,1006,1015h # ativa a consulta ao status do mouse
  }



  restore()
  {
    ? ?1000,1002,1006,1015l # desativa a consulta ao status do mouse
    [[ $stty ]] && stty $stty
    ? ?25h # torna o cursor visível
    ? $LINES,1H
    ? K # apaga até o final da linha
    (( $$ != BASHPID )) && exit
  }



  frame()
  {

    local b_{h,v,lt,rt,lb,rb} i bordah altura=$LINES largura=$COLUMNS linha=1 coluna=1

     b_h=${theme[h]}
     b_v=${theme[v]}
    b_lt=${theme[lt]}
    b_rt=${theme[rt]}
    b_lb=${theme[lb]}
    b_rb=${theme[rb]}
    b_lm=${theme[lm]}
    b_rm=${theme[rm]}

    printf -v bordah "%$((largura-2))s"
    bordah=${bordah// /$b_h}

    ? $((linha+0)),${coluna}H$b_lt$bordah$b_rt
    ? $((linha+1)),$((coluna+largura+3))H$b_v
    ? $((linha+1)),$(((COLUMNS-${#title})/2))H
    ? 1,33m
    printf $title
    ? 0m
    ? $((linha+1)),${coluna}H$b_v
    ? $((linha+2)),${coluna}H$b_lm$bordah$b_rm
    for ((i=3; i<altura-3; i++))
    {
      ? $((linha+i)),${coluna}H$b_v
      ? $((linha+i)),$((coluna+largura+3))H$b_v
    }
    ? $((linha+i+0)),${coluna}H$b_lm$bordah$b_rm
    ? $((linha+i+1)),${coluna}H$b_v
    ? $((linha+i+1)),$((coluna+largura+3))H$b_v
    ? $((linha+i+2)),${coluna}H$b_lb$bordah$b_rb

  }



  palette()
  {
    local i
    # 0 black
    # 1 red
    # 2 green
    # 3 yellow
    # 4 blue
    # 5 magenta
    # 6 cyan
    # 7 white
    for i in {1..7}
    {
      ? ${print_lin},$((COLUMNS-17+2*i))H
      ? 30,4${i}m
      ( ((i==defcolor)) && [[ ! $color ]] ) && printf "$selshape" || printf '  '
      ? 0m
    }
  }



  ? ()
  {
    [[ $2 ]] && local var="-v$2"
    printf $var "\x1b[${1//,/;}"
  }



  $autoexec && main



# EOF
